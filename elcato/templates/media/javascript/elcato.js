function elcatoSearchInit(path, data) {
    M.AutoInit();
        var elcatoSearchElement = document.querySelector("#search");
        function searchGo(item) {
            window.location.href = path + data[item];
        }

        var elcatoSearch = M.Autocomplete.init(elcatoSearchElement, {
            minLength: 1,
            onAutocomplete: searchGo
        });
        elcatoSearch.updateData(data);
    return true;
}

var start = setInterval(function() {
  if (searchData && M != undefined) {
      elcatoSearchInit(elcatopath, searchData);
      clearInterval(start);
      return true;
  } else {
      return false;
  }
}, 3000);
