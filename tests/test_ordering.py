import os
import re
import json
import helper
from elcato import settings
from elcato import build


def test_ordering_index():
    destination, config = helper.test_setup("ordering/", "elcato-json.yaml")
    source = os.path.dirname(os.path.abspath(__file__))
    root = os.path.abspath(f"{source}/../")

    build(root, destination, config)
    assert settings.TEMPLATE is not None
    index_path = f"{destination}index.json"
    with open(index_path, "r") as fp:
        value = json.loads(fp.read())
        assert value.get("title") == "Index"
        assert len(value.get("cards")) == 4
        dates = [p.get("date") for p in value.get("cards")]

        assert dates == [
            " 2018-10-10 12:00:00 UTC",
            " 2018-10-09 12:00:00 UTC",
            " 2018-10-08 12:00:00 UTC",
            " 2017-07-01 12:00:00 UTC",
        ]


def test_ordering_rss():
    destination, config = helper.test_setup("ordering/", "elcato-json.yaml")
    source = os.path.dirname(os.path.abspath(__file__))
    root = os.path.abspath(f"{source}/../")
    build(root, destination, config)
    assert settings.TEMPLATE is not None
    index_path = f"{destination}rss.xml"
    with open(index_path, "r") as fp:
        dates = re.findall("<pubDate>(.*?)</pubDate>", fp.read())
        assert dates == [
            'Sat, 01 Jul 2017 12:00:00 +0000',
            'Mon, 08 Oct 2018 12:00:00 +0000',
            'Tue, 09 Oct 2018 12:00:00 +0000',
            'Wed, 10 Oct 2018 12:00:00 +0000']
